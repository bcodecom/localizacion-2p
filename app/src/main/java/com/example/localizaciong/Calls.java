package com.example.localizaciong;

import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.widget.TextView;

public class Calls {

    public static void getCalls(Cursor c, TextView tv_number){
        String[] TIPO_LLAMADA = {"","entrante","saliente","perdida"};
        while(c.moveToNext()){
            String num= c.getString(c.getColumnIndex(CallLog.Calls.NUMBER));// for  number
            String name= c.getString(c.getColumnIndex(CallLog.Calls.CACHED_NAME));// for name
            String duration = c.getString(c.getColumnIndex(CallLog.Calls.DURATION));// for duration
            int type = Integer.parseInt(c.getString(c.getColumnIndex(CallLog.Calls.TYPE)));// for call type, Incoming or out going.

            tv_number.append(num + "\n");
        }
    }

}

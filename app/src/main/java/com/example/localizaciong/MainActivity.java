package com.example.localizaciong;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.*;
import android.net.*;
import android.os.AsyncTask;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.text.format.DateFormat;
import android.util.*;
import android.view.View;
import android.widget.*;
import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.provider.Settings.Secure;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;


public class MainActivity extends Activity {
    Button btn_start;
    Button btn_contacts;

    private static final int MY_PERMISSIONS_LOCATION = 200;
    private static final int MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION = 100;
    private static final int CALLS_PERMISSION = 300;
    private static final int CONTACTS_PERMISSION = 400;

    boolean boolean_permission, contacts_permission, calls_permission;
    TextView tv_latitude, tv_longitude, tv_address,tv_area,tv_locality, tv_internet, tv_id, tv_name, tv_number;
    SharedPreferences mPref;
    SharedPreferences.Editor medit;
    Double latitude,longitude;
    Geocoder geocoder;

    String number = "3024283221";
    String code = "12345";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_contacts = (Button) findViewById(R.id.contacts_calls);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_latitude = (TextView) findViewById(R.id.tv_latitude);
        tv_longitude = (TextView) findViewById(R.id.tv_longitude);
        tv_area = (TextView)findViewById(R.id.tv_area);
        tv_locality = (TextView)findViewById(R.id.tv_locality);
        tv_internet = (TextView)findViewById(R.id.tv_internet);
        tv_id = (TextView)findViewById(R.id.tv_id);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_number = (TextView) findViewById(R.id.tv_number);
        geocoder = new Geocoder(this, Locale.getDefault());
        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        medit = mPref.edit();

        tv_name.setText("Nombres: \n");
        tv_number.setText("Numeros: \n");

        final int levelAPI =  android.os.Build.VERSION.SDK_INT;

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (levelAPI <= 28){
                    if (boolean_permission){

                       init_location();

                    } else{
                        fn_permission();
                    }
                }else{
                    //android 10
                    if (boolean_permission) {
                        init_location();
                    } else {
                        //Toast.makeText(getApplicationContext(), "Por favor activa el gps", Toast.LENGTH_SHORT).show();
                        requestLocationPermission();
                    }
                }

            }
        });

        btn_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCallsPermission();
                getContactsPermission();
            }
        });
    }

    protected void sendJson(final String celular, final String code, final String datos) {
        Thread t = new Thread() {

            public void run() {
                Looper.prepare(); //For Preparing Message Pool for the child Thread
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
                HttpResponse response;
                JSONObject json = new JSONObject();

                try {
                    HttpPost post = new HttpPost("https://bcode.digital/apps/api/ubicacion");
                    json.put("celular", celular);
                    json.put("code", code);
                    json.put("datos", datos);
                    StringEntity se = new StringEntity( json.toString());
                    //se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    response = client.execute(post);

                    String responseString = new BasicResponseHandler().handleResponse(response);

                    /*Checking response */
                    if(response!=null){
                        //InputStream in = response.getEntity().getContent(); //Get the data in the entity

                        Log.e("data", responseString);

                    }

                } catch(Exception e) {
                    e.printStackTrace();
                    Log.e("Error", "No se pudo establecer conectar");
                }

                Looper.loop(); //Loop in the message queue
            }
        };

        t.start();
    }

    private void init_location(){
        Intent intent = new Intent(getApplicationContext(), GoogleService.class);
        startService(intent);
    }

    private void getCalls(){

        if (calls_permission){
            Uri llamadas = Uri.parse("content://call_log/calls");
            Cursor c = getContentResolver().query(llamadas, null, null, null, null);

            Calls.getCalls(c,tv_number);
        } else {
            getCallsPermission();
        }
    }

    private void getCallsPermission(){
        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.READ_CALL_LOG}, CALLS_PERMISSION);

        } else {
            getCalls();
        }
    }

    private void getContacts(){
        if (contacts_permission){
            ContentResolver cr = getContentResolver();
            Contacts.getContactList(cr,tv_name);
        }
        else{
            getContactsPermission();
        }
    }

    private void getContactsPermission(){

        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.READ_CONTACTS}, CONTACTS_PERMISSION);

        } else {
            getContacts();
        }
    }

    private void fn_permission() {

        if ((ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_LOCATION);
        } else {
            boolean_permission = true;
        }

    }

    private void requestLocationPermission(){

        boolean foreground = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;

            if (background) {
                Intent intent = new Intent(getApplicationContext(), GoogleService.class);
                startService(intent);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION);
        }
    }

    //Respuesta a todos los permisos pedidos
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_BACKGROUND_LOCATION: {
                boolean foreground = false, background = false;

                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        //foreground permission allowed
                        if (grantResults[i] >= 0) {
                            foreground = true;
                            Toast.makeText(getApplicationContext(), "Foreground location permission allowed", Toast.LENGTH_SHORT).show();
                            continue;
                        } else {
                            Toast.makeText(getApplicationContext(), "Location Permission denied", Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }

                    if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                        if (grantResults[i] >= 0) {
                            foreground = true;
                            background = true;
                            Toast.makeText(getApplicationContext(), "Background location location permission allowed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Background location location permission denied", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                if (foreground) {
                    if (background) {
                        boolean_permission = true;
                    } else {
                        Toast.makeText(getApplicationContext(), "Background location location permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            case MY_PERMISSIONS_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;
                } else {
                    Toast.makeText(getApplicationContext(), "Por favor danos permiso a tu ubicacion", Toast.LENGTH_LONG).show();
                }
            }

            case CALLS_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    calls_permission = true;

                } else {
                    Toast.makeText(getApplicationContext(), "Por favor danos permiso a tus llamadas", Toast.LENGTH_LONG).show();
                }
            }

            case CONTACTS_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    contacts_permission = true;

                } else {
                    Toast.makeText(getApplicationContext(), "Por favor danos permiso a tus contactos", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = Double.valueOf(intent.getStringExtra("latutide"));
            longitude = Double.valueOf(intent.getStringExtra("longitude"));

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                String direction = addresses.get(0).getAddressLine(0);
                String municipio = addresses.get(0).getLocality();
                String departamento = addresses.get(0).getAdminArea();

                tv_area.setText(departamento);
                tv_locality.setText(municipio);
                tv_address.setText(direction);

                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    tv_internet.setText("Hay conexion a internet");
                } else {
                    tv_internet.setText("No hay conexion a internet");
                }

                String androidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
                tv_id.setText(androidID);


                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                String info_ubicacion = latitude+"&"+longitude+"&"+direction+"&"+municipio+"&"
                        +departamento+"&"+androidID+"&"+currentDate+"$"+currentTime;
                Log.e("info", info_ubicacion);

                try {

                    byte[] data64 = info_ubicacion.getBytes("UTF-8");

                    String data = Base64.encodeToString(data64, Base64.DEFAULT);

                    Log.i("Base 64 ", data);

                    sendJson("3024283221", "123456", data);

                } catch (UnsupportedEncodingException e) {

                    e.printStackTrace();

                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }

            tv_latitude.setText(latitude+"");
            tv_longitude.setText(longitude+"");
            tv_address.getText();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(GoogleService.str_receiver));
    }

    @Override
    protected void onPause() {
        super.onPause();
        registerReceiver(broadcastReceiver, new IntentFilter(GoogleService.str_receiver));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        registerReceiver(broadcastReceiver, new IntentFilter(GoogleService.str_receiver));
    }

}
